import numpy as np
import numpy.testing as npt
import scipy as sp
import scipy.linalg as la
import unittest
from magnusdecomp import Ups41, Ups41Kr, Ups62, Ups62Kr, Ups63, Ups63Kr, Ups63Comm, Ups63CommKr
from split import rkn
from magnussplit import MS_WE2017, MSC66, MSC651, MSC652, MSC431, MSC432, MSC433, MSC434
from rk import RKGL63, RK67
from commfree import CF63NOKr, CF43Kr


class TestAutWaveEq(unittest.TestCase):
    def setUp(self):
        n = 10
        R = np.random.rand(n, n)
        O = np.zeros((n, n))
        Id = np.identity(n)
        self.Idf = lambda t: Id
        self.M = R@R.T - n * n * np.identity(n)
        self.Mf = lambda t: self.M
        self.Aaut = np.array(np.bmat([[O, Id], [self.Mf(0), O]]))
        self.v = np.random.rand(n, 1)
        self.w = np.random.rand(n, 1)
        iv = np.vstack((self.v, self.w))
        self.T0 = 0
        self.Tf = 5
        self.n_steps = 500

        # Non-exp methods should give the exact solution in the autonomous case
        self.lowertol = 1e-6
        self.expected = la.expm(self.Aaut * (self.Tf - self.T0)) @ iv

    def test_RKGL63(self):
        actual = RKGL63((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_RK44(self):
        actual = RK44((self.Mf, self.Idf), (self.v, self.w),
                      [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

#    def test_RK67(self):
#        actual = RK67((self.Mf, self.Idf), (self.v, self.w),
#                      [self.T0, self.Tf], self.n_steps)()
#        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
#        npt.assert_allclose(actual, self.expected)

#    def test_CF43Kr(self):
#        actual = CF43Kr((self.Mf, self.Idf), (self.v, self.w),
#                        [self.T0, self.Tf], self.n_steps)()
#        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
#        npt.assert_allclose(actual, self.expected)

    def test_CF63NOKr(self):
        actual = CF63NOKr((self.Mf, self.Idf), (self.v, self.w),
                          [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_Ups41(self):
        actual = Ups41((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_Ups41Kr(self):
        actual = Ups41Kr((self.Mf, self.Idf), (self.v, self.w),
                         [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_Ups62(self):
        actual = Ups62((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_Ups62Kr(self):
        actual = Ups62Kr((self.Mf, self.Idf), (self.v, self.w),
                         [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_Ups63(self):
        actual = Ups63((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_Ups63Kr(self):
        actual = Ups63Kr((self.Mf, self.Idf), (self.v, self.w),
                         [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_Ups63Comm(self):
        actual = Ups63Comm((self.Mf, self.Idf), (self.v, self.w),
                           [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_Ups63CommKr(self):
        actual = Ups63CommKr((self.Mf, self.Idf), (self.v, self.w),
                             [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_MS_WE2017(self):
        actual = MS_WE2017((self.Mf, self.Idf), (self.v, self.w),
                           [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_MSC66(self):
        actual = MSC66((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_MSC651(self):
        actual = MSC651((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_MSC652(self):
        actual = MSC652((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_MSC431(self):
        actual = MSC431((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-3)

    def test_MSC432(self):
        actual = MSC432((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_MSC433(self):
        actual = MSC433((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_MSC434(self):
        actual = MSC434((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_RKNb611(self):
        actual = RKNb611((self.Mf, self.Idf), (self.v, self.w),
                         [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_RKNb46(self):
        actual = RKNb46((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)
