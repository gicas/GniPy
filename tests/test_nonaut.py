import numpy as np
import numpy.testing as npt
import scipy as sp
import scipy.linalg as la
import unittest
from magnusdecomp import Ups41, Ups41Kr, Ups62, Ups62Kr, Ups63, Ups63Kr, Ups63Comm, Ups63CommKr
from split import RKNb46, RKNb611
from magnussplit import MS_WE2017, MSC431, MSC432, MSC433, MSC434
from rk import RKGL63, RK67, RK44
from commfree import CF63NOKr, CF43Kr


class TestNonAutWaveEq(unittest.TestCase):

    def setUp(self):
        def nonautwave(x, eps, delta):
            h = np.abs(x[1] - x[0])
            n = len(x)
            L = (np.diagflat(-2 * np.ones((n, 1)), 0) +
                 np.diagflat(np.ones((n - 1, 1)), 1) +
                 np.diagflat(np.ones((n - 1, 1)), -1) +
                 np.diagflat(1, n - 1) + np.diagflat(1, -n + 1))
            L = L / (h * h)
            return lambda t: L - np.diagflat(x**2 + eps * np.cos(delta * t) * x**2)

        n = 10
        X0 = -10
        Xf = 10
        x = np.linspace(X0, Xf, num=n)
        Id = np.identity(n)
        self.Idf = lambda t: Id
        delta = 1
        eps = 1
        self.Mf = nonautwave(x, eps, delta)
        self.v = np.exp(-0.5 * x**2).reshape(-1, 1)
        self.w = 0.0 * self.v
        self.T0 = 0
        self.Tf = sp.pi * 3.4
        self.n_steps = 200

        self.expected = np.array([0.000014339416771,
                                  0.000764980668231,
                                  0.003676539234222,
                                  -0.213527037621829,
                                  -0.457107962291335,
                                  -0.457107962291334,
                                  -0.213527037621829,
                                  0.003676539234222,
                                  0.000764980668231,
                                  0.000014339416771,
                                  0.000224268957328,
                                  0.007650898001954,
                                  0.200307608352652,
                                  1.093141264639347,
                                  0.314887850502139,
                                  0.314887850502139,
                                  1.093141264639348,
                                  0.200307608352652,
                                  0.007650898001954,
                                  0.000224268957328]).reshape(-1, 1)

    def test_RKGL63(self):
        actual = RKGL63((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

    def test_RK44(self):
        actual = RK44((self.Mf, self.Idf), (self.v, self.w),
                      [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

#    def test_RK67(self):
#        actual = RK67((self.Mf, self.Idf), (self.v, self.w),
#                      [self.T0, self.Tf], self.n_steps)()
#        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
#        npt.assert_allclose(actual, self.expected, rtol=1e-5)

#    def test_CF43Kr(self):
#        actual = CF43Kr((self.Mf, self.Idf), (self.v, self.w),
#                        [self.T0, self.Tf], self.n_steps)()
#        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=6)
#        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_CF63NOKr(self):
        actual = CF63NOKr((self.Mf, self.Idf), (self.v, self.w),
                          [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

    def test_Ups41(self):
        actual = Ups41((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=6)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_Ups41Kr(self):
        actual = Ups41Kr((self.Mf, self.Idf), (self.v, self.w),
                         [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=6)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_Ups62(self):
        actual = Ups62((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

    def test_Ups62Kr(self):
        actual = Ups62Kr((self.Mf, self.Idf), (self.v, self.w),
                         [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

    def test_Ups63(self):
        actual = Ups63((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

    def test_Ups63Kr(self):
        actual = Ups63Kr((self.Mf, self.Idf), (self.v, self.w),
                         [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

    def test_Ups63Comm(self):
        actual = Ups63Comm((self.Mf, self.Idf), (self.v, self.w),
                           [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

    def test_Ups63CommKr(self):
        actual = Ups63CommKr((self.Mf, self.Idf), (self.v, self.w),
                             [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

    def test_MSC431(self):
        actual = MSC431((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_MSC432(self):
        actual = MSC432((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_MSC433(self):
        actual = MSC433((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_MSC434(self):
        actual = MSC434((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected, rtol=1e-4)

    def test_RKNb611(self):
        actual = RKNb611((self.Mf, self.Idf), (self.v, self.w),
                         [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected,  rtol=1e-5)

    def test_RKNb46(self):
        actual = RKNb46((self.Mf, self.Idf), (self.v, self.w),
                        [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0, decimal=4)
        npt.assert_allclose(actual, self.expected,  rtol=1e-4)

    def test_MSC66(self):
        actual = MSC66((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_MSC651(self):
        actual = MSC651((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_MSC652(self):
        actual = MSC652((self.Mf, self.Idf), (self.v, self.w),
                       [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected)

    def test_MS_WE2017(self):
        actual = MS_WE2017((self.Mf, self.Idf), (self.v, self.w),
                           [self.T0, self.Tf], self.n_steps)()
        npt.assert_almost_equal(la.norm(actual - self.expected), 0)
        npt.assert_allclose(actual, self.expected, rtol=1e-5)

