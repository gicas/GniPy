#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 10:15:57 2017

@author: nik
"""

import numpy as np
import scipy as sp
import scipy.linalg as la
from numpy import zeros, ones, eye
from numpy.random import rand
from krypy.utils import arnoldi
from expms import expmkr


def sympj(n):
    return np.block([[np.zeros((n, n)), np.eye(n)],
                     [-1 * np.eye(n), np.zeros((n, n))]])
    

def omega(a, b):
    n = len(a)//2
    J = sympj(n)
    return a.T @ J @ b


def block(list):
    return np.array(np.bmat(list))


dim = 128
Id = eye(dim)
Os = zeros((dim, dim))
R = rand(dim, dim)
M = R@R.T - dim*dim*Id

J = sympj(dim)
A = block([[Os, Id], [M, Os]])
h = 1/10000
A = h * A

experr = 0.
krerr = 0.
tlrerr = 0.
ntests = 1
for num in range(ntests):
    a = rand(2*dim, 1)
    b = rand(2*dim, 1)

    kra = expmkr(A, a)
    expa = la.expm(A) @ a
    tlra = la.expm3(A, 10) @ a

    krb = expmkr(A, b)
    expb = la.expm(A) @ b
    tlrb = la.expm3(A, 10) @ b

    ref = omega(a, b)
    expom = omega(expa, expb)
    krom = omega(kra, krb)
    tlrom = omega(tlra, tlrb)

    experr += la.norm(ref-expom)
    krerr += la.norm(ref-krom)
    tlrerr += la.norm(ref-tlrom)

experr = experr/ntests
krerr = krerr/ntests   #  depends on the relation between the norm of A and norm of v
tlrom = tlrom/ntests

print(experr)
print(krerr)
print(tlrom)