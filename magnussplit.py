import numpy as np
from core import Integrator, GaussLegendreInterp, GLI4, GLI6


def eyes(shp):
    m, n = shp
    return np.eye(m, n)


class MagnusSplitting(Integrator, GaussLegendreInterp):
    def __init__(self, *args, **kwargs):
        super(MagnusSplitting, self).__init__(*args, **kwargs)
        self.T = np.array([[0, 1, 0],
                           [-np.sqrt(15) / 3, 0, np.sqrt(15) / 3],
                           [10 / 3, -20 / 3, 10 / 3]
                           ])

    def step_setup(self):
        self.M = [self.f(self.t + qn * self.h) for qn in self.qnodes]
        self.N = [self.g(self.t + qn * self.h) for qn in self.qnodes]


class MagnusSplittingCommutator(MagnusSplitting):
    def __init__(self, *args, **kwargs):
        super(MagnusSplittingCommutator, self).__init__(*args, **kwargs)
        self.veccost = None
        self.matcost = None
        self.y = None
        self.a = None
        self.x = None

    def step_body(self):
        for a, x, y in zip(self.a, self.x, self.y):
            A = (y[0] * self.M[0]
                 + y[1] * self.M[1]
                 + y[2] * self.M[2])
            B = (a * self.N[1]  # currently supposed to be constant
                 + self.h**2 * (x[0] * self.M[0]
                                + x[1] * self.M[1]
                                + x[2] * self.M[2])
                 )
            self.W += self.h * A @ self.V
            self.V += self.h * B @ self.W


class MS_WE2017(MagnusSplitting, GLI6):
    texname = r'$\Sigma_{11}^{[6]}$'
    name = r'MS11'
    line = '-.'
    plotstyle = dict(linestyle=line, marker='h', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(MS_WE2017, self).__init__(*args, **kwargs)
        self.veccost = 11
        self.matcost = 22

        self.b = np.array(
            [0.109548777487881,
             -0.0839558265772011,
             0.0853628944878278,
             0.192996405137533,
             -0.0819019090921481,
             0.277949658556107,
             0.277949658556107,
             -0.0819019090921481,
             0.192996405137533,
             0.0853628944878278,
             -0.0839558265772011,
             0.109548777487881])
        self.a = np.array(
            [[0, -0.0467849428934380, 0],
             [0.0324409409589770, 0.0467879085392910, -0.0324409409589770],
             [0.0736541454842830, 0.204632917360118, -0.0736541454842830],
             [0.849014173832242, -1.26064146482527, 0.676471413442501],
             [-0.135581449060479, 0.182282214200409, -0.0840123956106620],
             [-1.02811396482583, 2.19189117968222, -1.02811396482583],
             [-0.0840123956106620, 0.182282214200409, -0.135581449060479],
             [0.676471413442501, -1.26064146482527, 0.849014173832242],
             [-0.0736541454842830, 0.204632917360118, 0.0736541454842830],
             [-0.0324409409589770, 0.0467879085392910, 0.0324409409589770],
             [0, -0.0467849428934380, 0],
             [0, 0, 0]])

    def step_body(self):
        for a, b in zip(self.a, self.b):
            A = (a[0] * self.M[0]
                 + a[1] * self.M[1]
                 + a[2] * self.M[2])
            B = b * self.N[1]  # currently supposed to be constant
            self.V += self.h * B @ self.W
            self.W += self.h * A @ self.V


class MSC66(MagnusSplittingCommutator, GLI6):
    texname = r'$\Sigma_{5}^{[6]}$'
    name = r'MSC5'
    line = '-.'
    plotstyle = dict(linestyle=line, marker='h', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(MSC66, self).__init__(*args, **kwargs)
        self.veccost = 7
        self.matcost = None

        y11 = 0.0514582866001796167314312225923
        y12 = 0.0076075199691114461803317908
        y13 = -0.00731085265710648877765674719
        y21 = 0.115208380066487049935235444074
        y22 = 0.064270386358966981925252346
        y23 = 0.04000185283886262212914664896
        y31 = 1/3
        y32 = 0.0549763132972774670634966864
        y33 = 0.008975666484910533315176764897

        self.y = np.array(
            [[y11, -y12, y13],
             [y21, -y22, y23],
             [y31, -y32, y33],
             [y31, y32, y33],
             [y21, y22, y23],
             [y11, y12, y13]
             ])@self.T

        self.a = np.array(
            [0.0580855231046988793361036562709,
             0.269824135673790464236406672507,
             0.344180682443021312854979342445,
             0.269824135673790464236406672507,
             0.0580855231046988793361036562709,
             0
             ])

        x21 = -0.00178423046853071302651948275121
        x22 = -0.00036890366276806058644501651
        self.x = np.array([
            [0, 0, 0],
            [x21, -x22, 0],
            [0, 0, 0],
            [x21, x22, 0],
            [0, 0, 0],
            [0, 0, 0]
        ])@(-2 * self.T)

class MSC651(MagnusSplittingCommutator, GLI6):
    texname = r'$\Sigma_{5,2}^{[6]}$'
    name = r'MSC5'
    line = '-.'
    plotstyle = dict(linestyle=line, marker='h', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(MSC651, self).__init__(*args, **kwargs)
        self.veccost = 6
        self.matcost = None

        y11 = 0.56424861631106376214537683529008182030188418
        y12 = 0.17669075096896369912143716816582487475948700
        y13 = -0.313516035197004581386177124929635585365895
        y21 = -0.2393627021773294286793754464951913653581447
        y22 = 0.057187275163375406372949776841886455635781481
        y23 = 0.613049370231531664397859228558954163360836639
        y31 = 0.350228171732531333067997222410219090112521225
        y32 = 0
        y33 = -0.515733336735720832690030873925303822656549809

        self.y = np.array(
            [[0, 0, 0],
             [y11, -y12, y13],
             [y21, -y22, y23],
             [y31, -y32, y33],
             [y21, y22, y23],
             [y11, y12, y13]
             ])@self.T

        self.a = np.array(
            [0.18685656311551125972492223836210406277414,
             0.55205816605147814842610330386678482120980017,
             -0.23891472916698940817722063484025666774393541,
             -0.23891472916698940817722063484025666774393541,
             0.55205816605147814842610330386678482120980017,
             0.18685656311551125972492223836210406277414,
             ])

        x11 = -5.4144546382263551069104926023713179731620 * 1e-26
        x12 = 0.002294120141001916616005143395346560983003
        x13 = 1/1000
        self.x = np.array([
            [x11, -x12, x13],
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            [x11, x12, x13]
        ])@(-2 * self.T)

class MSC652(MagnusSplittingCommutator, GLI6):
    texname = r'$\Sigma_{5,2}^{[6]}$'
    name = r'MSC5'
    line = '-.'
    plotstyle = dict(linestyle=line, marker='h', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(MSC652, self).__init__(*args, **kwargs)
        self.veccost = 6
        self.matcost = None

        a1 = 0.4020196038964999834667407562746757263281210
        x11 = 5.64715489087290449910314862137337559720276988 * 1e-26
        x12 = -0.0009201656925666982840702980574642404868179
        x13 = 0
        y11 = 0.911084237567661521857460069462681888248920856
        y12 = 0.08926839448053478635700141521732726648276119
        y13 = -0.02844705845687261447733612016300474736175
        a2 = 0.5329396856308538150258756295019077893504284
        y21 = 0.1740059542332660799009380988351693085123139
        y22 = -0.075685506226830655186774723591742677596622
        y23 = 0.0344791511992324146288360211745620719734431
        a3 = -0.434959289527353798492616385776583515678549
        y31 = -1.170180383601855203516796336595702393522469
        y32=0
        y33 = 0.07126914784861373303033353131021868410995281

        self.y = np.array(
            [[0, 0, 0],
             [y11, -y12, y13],
             [y21, -y22, y23],
             [y31, -y32, y33],
             [y21, y22, y23],
             [y11, y12, y13]
             ])@self.T

        self.a = np.array(
            [a1,
             a2,
             a3,
             a3,
             a2,
             a1,
             ])

        x11 = -5.4144546382263551069104926023713179731620 * 1e-26
        x12 = 0.002294120141001916616005143395346560983003
        x13 = 1/1000
        self.x = np.array([
            [x11, -x12, x13],
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
            [x11, x12, x13]
        ])@(-2 * self.T)

class MSC431(MagnusSplittingCommutator, GLI6):
    texname = r'$\Phi_{31}^{[4]}$'
    name = r'MSC5'
    line = '-'
    plotstyle = dict(linestyle=line, marker='^', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(MSC431, self).__init__(*args, **kwargs)
        self.veccost = 3
        self.matcost = None

        self.y = np.array(
            [[0, 0, 0],
             [1 / 2, -1 / 8, 1 / 24],
             [1 / 2, +1 / 8, 1 / 24]]
        ) @self.T

        self.a = np.array(
            [1 / 6,
             2 / 3,
             1 / 6])
        self.x = np.array([
            [-1 / 144, 0, 7 / 8640],
            [0, 0, 0],
            [-1 / 144, 0, 7 / 8640]]
        ) @(-2 * self.T)


class MSC432(MagnusSplittingCommutator, GLI6):
    texname = r'$\Phi_{32}^{[4]}$'
    name = r'MSC5'
    line = '-'
    plotstyle = dict(linestyle=line, marker='*', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(MSC432, self).__init__(*args, **kwargs)
        self.veccost = 3
        self.matcost = None

        self.y = np.array(
            [[0, 0, 0],
             [1 / 2, -1 / 8, 1 / 24],
             [1 / 2, +1 / 8, 1 / 24]]
        ) @self.T

        self.a = np.array(
            [1 / 6,
             2 / 3,
             1 / 6])
        self.x = np.array([
            [0, 0, 0],
            [-1 / 72, 0, 7 / 4320],
            [0, 0, 0]]
        ) @(-2 * self.T)


class MSC433(MagnusSplittingCommutator, GLI6):
    texname = r'$\Phi_{33}^{[4]}$'
    name = r'MSC5'
    line = '-'
    plotstyle = dict(linestyle=line, marker='s', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(MSC433, self).__init__(*args, **kwargs)
        self.veccost = 4
        self.matcost = None

        self.y = np.array(
            [[0, 0, 0],
             [1 / 2, -1 / 8, 1 / 24],
             [1 / 2, +1 / 8, 1 / 24]]
        ) @self.T

        self.a = np.array(
            [1 / 6,
             2 / 3,
             1 / 6])
        xx = 1/24
        self.x = np.array([
            [1 / 240, -1 / 720, xx],
            [-1 / 45, 0, (7-8640*xx) / 4320],
            [1 / 240, 1 / 720, xx]]
        ) @(-2 * self.T)


class MSC434(MagnusSplittingCommutator, GLI6):
    texname = r'$\Phi_{34}^{[4]}$'
    name = r'MSC5'
    line = '-'
    plotstyle = dict(linestyle=line, marker='o', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(MSC434, self).__init__(*args, **kwargs)
        self.veccost = 4
        self.matcost = None

        self.y = np.array(
            [[(3 - np.sqrt(3)) / 6, -1 / 12, (5 - np.sqrt(3)) / 120],
             [np.sqrt(3) / 3, 0, np.sqrt(3) / 60],
             [(3 - np.sqrt(3)) / 6, 1 / 12, (5 - np.sqrt(3)) / 120]]
        ) @self.T

        self.a = np.array(
            [1 / 2,
             1 / 2,
             0])
        self.x = np.array([
            [(-2 + np.sqrt(3)) / 48, 0, (-2 + np.sqrt(3)) / 960],
            [(-2 + np.sqrt(3)) / 48, 0, (-2 + np.sqrt(3)) / 960],
            [0, 0, 0]]
        ) @(-2 * self.T)
