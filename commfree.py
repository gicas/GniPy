import numpy as np
from core import Integrator, GaussLegendreInterp, GLI4, GLI6
from expms import expmkr


def eyes(shp):
    m, n = shp
    return np.eye(m, n)


class CommutatorFree(Integrator, GaussLegendreInterp):
    def step_setup(self):
        self.sysvals = np.array(
            [[f(self.t + qn * self.h) for qn in self.qnodes] for f in self.sys])

    def step_body(self):
        for stage in self.w:
            F = np.sum(np.array([c * M for (c, M) in zip(stage, self.sysvals[0])]),
                       axis=0)
            G = np.sum(np.array([c * M for (c, M) in zip(stage, self.sysvals[1])]),
                       axis=0)
            T = self.h * np.block([[self.O, G],
                                   [F, self.O]])
            self.result = np.vstack((self.V, self.W))
            self.result, step_cost = expmkr(T, self.result)
            self.veccost += step_cost / self.nsteps
            self.V, self.W = np.split(self.result, 2)


class CF43Kr(CommutatorFree, GLI4):
    texname = texname = r'$\mathrm{CF}_{3_Kr}^{[4]}$'
    name = r'CF43Kr'
    line = ':'
    plotstyle = dict(linestyle=line, marker='s', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(CF43Kr, self).__init__(*args, **kwargs)
        self.matcost = None
        self.veccost = 0
        # From Alvermann & Blanes, Casas, Thalhammer; w = FGQ
        a = 37 / 240 - 10 * np.sqrt(5 / 3) / 87
        b = 37 / 240 + 10 * np.sqrt(5 / 3) / 87
        self.w = np.array(
            [[a, -1 / 30, b],
             [-11 / 360, 23 / 45, -11 / 360],
             [b, -1 / 30, a]])
        self.w = np.flip(self.w, 0)


class CF63NOKr(CommutatorFree, GLI6):
    texname = texname = r'$\mathrm{CF}_{6NO_Kr}^{[6]}$'
    name = r'CF63NOKr'
    line = ':'
    plotstyle = dict(linestyle=line, marker='*', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(CF63NOKr, self).__init__(*args, **kwargs)
        self.matcost = None
        self.veccost = 0
        self.w = np.array(
            [[0.215838996975768, -0.076717964591551, 0.020878967615784],
             [-0.080897796320853, -0.178747217537158, 0.032263366431047],
             [0.180628460055830, 0.477687404350931, -0.090934216979798],
             [-0.090934216979798, 0.477687404350931,  0.180628460055830],
             [0.032263366431047, -0.178747217537158, -0.080897796320853],
             [0.020878967615784, -0.076717964591551, 0.215838996975768]])
