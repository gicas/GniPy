import benchmarks as bms
from rk import rkgl, rk76
from split import rkn
from magnussplit import msp
from magnusdecomp import ups62, ups63, ups63c, ups62kr, ups63kr
from desystems import mathieu, idf, wave
from scipy import pi
import numpy as np
from numpy.random import rand


methods = [ups62, ups63, ups63c, rkn, msp, rkgl, rk76]
methods2 = [ups62, ups63, ups62kr, ups63kr, msp]
methods3 = [ups62, ups63, msp]

Idf = idf(1)
Mf = mathieu(5, 1)
sys = (Mf, Idf)
iv = (np.array([[1, 0]]), np.array([[0, 1]]))

reference = rkn(sys, iv, [0, pi], 7500)()
steps = np.array([10**k for k in np.arange(0, 2.8, 0.2)]).astype(int)
bms.compare(methods, sys, iv, [0, pi], steps, reference)

bms.mathieu_eig(methods)
bms.mathieu_omega(methods)

tspan = [0, 100*pi]
nsteps = 1000

iv1 = (rand(1,1), rand(1,1))
iv2 = (rand(1,1), rand(1,1))
Mf = mathieu(3.5, 5)
sys = (Mf, Idf)
bms.sympmat(methods, sys, iv1, iv2, tspan, nsteps, 'nores')
bms.sympmatvec(methods, sys, iv1, iv2, tspan, nsteps, 'nores')
bms.sympvec(methods2, sys, iv1, iv2, tspan, nsteps, 'nores')

Mfr = mathieu(2, 5)
sys = (Mfr, Idf)
bms.sympmat(methods, sys, iv1, iv2, tspan, nsteps, 'res')
bms.sympmatvec(methods, sys, iv1, iv2, tspan, nsteps, 'res')
bms.sympvec(methods2, sys, iv1, iv2, tspan, nsteps, 'res')

#
#  Wave equation
#
tspan = [0, 200*pi]
nsteps = 1000

dim = 128
iv1 = (rand(dim, 1), rand(dim, 1))
iv2 = (rand(dim, 1), rand(dim, 1))
Idf = idf(dim)
x = np.linspace(-10, 10, dim)

eps_low = 0.2
delta_low = 0.2
Mf_low = wave(x, eps_low, delta_low)
syswave = (Mf_low, Idf)
bms.sympmatvec(methods3, syswave, iv1, iv2, tspan, nsteps, 'wave_low')
bms.sympmat(methods3, syswave, iv1, iv2, tspan, nsteps, 'wave_low')
bms.sympvec(methods2, syswave, iv1, iv2, tspan, nsteps, 'wave_low')

#eps_high = 1
#delta_high = 1
#Mf_high = wave(x, eps_high, delta_high)
#syswave = (Mf_high, Idf)
#bms.sympmatvec(methods, syswave, iv1, iv2, tspan, nsteps, 'wave_high')
#bms.sympmat(methods, syswave, iv1, iv2, tspan, nsteps, 'wave_high')
#bms.sympvec(methods2, syswave, iv1, iv2, tspan, nsteps, 'wave_high')
#
#eps_high = 2
#delta_high = 2
#Mf_high = wave(x, eps_high, delta_high)
#syswave = (Mf_high, Idf)
#bms.sympmatvec(methods, syswave, iv1, iv2, tspan, nsteps, 'wave_very_high')
#bms.sympmat(methods, syswave, iv1, iv2, tspan, nsteps, 'wave_very_high')
#bms.sympvec(methods2, syswave, iv1, iv2, tspan, nsteps, 'wave_very_high')