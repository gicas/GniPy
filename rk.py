import numpy as np
from core import Integrator, GLI6


def eyes(shp):
    m, n = shp
    return np.eye(m, n)


class RungeKutta(Integrator):
    def __init__(self, *args, **kwargs):
        super(RungeKutta, self).__init__(*args, **kwargs)
        self.a = np.array(
            [[5 / 36, 2 / 9 - np.sqrt(15) / 15, 5 / 36 - np.sqrt(15) / 30],
             [5 / 36 + np.sqrt(15) / 24, 2 / 9, 5 / 36 - np.sqrt(15) / 24],
             [5 / 36 + np.sqrt(15) / 30, 2 / 9 + np.sqrt(15) / 15, 5 / 36]])
        self.b = [5 / 18, 8 / 18, 5 / 18]


class RKGL63(RungeKutta, GLI6):
    texname = r'$\mathrm{RKGL}_{3}^{[6]}$'
    name = r'RKGL'
    line = '-'
    plotstyle = dict(linestyle=line, marker='s', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(RKGL63, self).__init__(*args, **kwargs)
        iter_per_step = 6
        self.matcost = 6 * iter_per_step
        self.veccost = 3 * iter_per_step

    def step_setup(self):
        self.F = [self.f(self.t + qn * self.h) for qn in self.qnodes]
        self.shp = self.F[1].shape

    def step_body(self):
        A1 = np.block([[self.O, self.h * self.Id],
                       [self.h * self.F[0], self.O]])
        A2 = np.block([[self.O, self.h * self.Id],
                       [self.h * self.F[1], self.O]])
        A3 = np.block([[self.O, self.h * self.Id],
                       [self.h * self.F[2], self.O]])

        AA = np.block([[self.a[0, 0] * A1, self.a[0, 1] * A2, self.a[0, 2] * A3],
                       [self.a[1, 0] * A1, self.a[1, 1] * A2, self.a[1, 2] * A3],
                       [self.a[2, 0] * A1, self.a[2, 1] * A2, self.a[2, 2] * A3]])
        ba = np.block([self.b[0] * A1, self.b[1] * A2, self.b[2] * A3])
        Id = np.eye(self.shp[0] * 2)
        msol = np.linalg.solve(
            np.eye(self.shp[0] * 2 * 3) - AA, np.block([Id, Id, Id]).T)
        temp = Id + ba @ msol
        result = temp @ np.vstack((self.V, self.W))
        self.V, self.W = np.vsplit(result, 2)


class RK67(RungeKutta):
    texname = r'$\mathrm{RK}_{7}^{[6]}$'
    name = r'RK67'
    line = '-'

    plotstyle = dict(linestyle=line, marker='D', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(RK67, self).__init__(*args, **kwargs)
        self.matcost = 14
        self.veccost = 7

        self.A6 = self.A(self.t0)

        self.rq5 = np.sqrt(5)
        self.a21 = (5 - self.rq5) / 10
        self.a31 = -self.rq5 / 10
        self.a32 = (5 + 2 * self.rq5) / 10
        self.a41 = (-15 + 7 * self.rq5) / 20
        self.a42 = (-1 + self.rq5) / 4
        self.a43 = (15 - 7 * self.rq5) / 10
        self.a51 = (5 - self.rq5) / 60
        self.a52 = 0
        self.a53 = 1 / 6
        self.a54 = (15 + 7 * self.rq5) / 60
        self.a61 = (5 + self.rq5) / 60
        self.a62 = 0
        self.a63 = (9 - 5 * self.rq5) / 12
        self.a64 = 1 / 6
        self.a65 = (-5 + 3 * self.rq5) / 10
        self.a71 = 1 / 6
        self.a72 = 0
        self.a73 = (-55 + 25 * self.rq5) / 12
        self.a74 = (-25 - 7 * self.rq5) / 12
        self.a75 = 5 - 2 * self.rq5
        self.a76 = (5 + self.rq5) / 2
        self.c2 = (5 - self.rq5) / 10
        self.c3 = (5 + self.rq5) / 10
        self.c4 = self.c2
        self.c5 = self.c3
        self.c6 = self.c2
        self.c7 = 1
        self.b1 = 1 / 12
        self.b2 = 0
        self.b3 = 0
        self.b4 = 0
        self.b5 = 5 / 12
        self.b6 = 5 / 12
        self.b7 = 1 / 12

    def A(self, t):
        return np.block([[self.O, self.g(t)],
                         [self.f(t), self.O]])

    def step_setup(self):
        self.X = np.vstack((self.V, self.W))
        self.A0 = self.A6
        self.A1 = self.A(self.t + self.c2 * self.h)
        self.A2 = self.A(self.t + self.c3 * self.h)
        self.A3 = self.A(self.t + self.c4 * self.h)
        self.A4 = self.A(self.t + self.c5 * self.h)
        self.A5 = self.A(self.t + self.c6 * self.h)
        self.A6 = self.A(self.t + self.c7 * self.h)

    def step_body(self):
        K1 = self.A0 @ self.X
        K2 = self.A1 @ (self.X + self.h *
                        self.a21 * K1)
        K3 = self.A2 @ (self.X + self.h *
                        (self.a31 * K1 + self.a32 * K2))
        K4 = self.A3 @ (self.X + self.h *
                        (self.a41 * K1 + self.a42 * K2 + self.a43 * K3))
        K5 = self.A4 @ (self.X + self.h *
                        (self.a51 * K1 + self.a52 * K2
                         + self.a53 * K3 + self.a54 * K4))
        K6 = self.A5 @ (self.X + self.h *
                        (self.a61 * K1 + self.a62 * K2
                         + self.a63 * K3 + self.a64 * K4 + self.a65 * K5))
        K7 = self.A6 @ (self.X + self.h *
                        (self.a71 * K1 + self.a72 * K2
                         + self.a73 * K3 + self.a74 * K4 + self.a75 * K5
                         + self.a76 * K6))
        self.X = (self.X + self.h *
                  (self.b1 * K1 + self.b2 * K2 + self.b3 * K3 + self.b4 *
                   K4 + self.b5 * K5 + self.b6 * K6 + self.b7 * K7))
        self.V, self.W = np.split(self.X, 2)


class RK44(RungeKutta):
    texname = r'$\mathrm{RK}_{4}^{[4]}$'
    name = r'RK44'
    line = '-'

    plotstyle = dict(linestyle=line, marker='D', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(RK44, self).__init__(*args, **kwargs)
        self.matcost = 8
        self.veccost = 4

        self.A4 = self.A(self.t0)

        self.rq5 = np.sqrt(5)

        self.a21 = 1 / 3
        self.a31 = -1 / 3
        self.a32 = 1
        self.a41 = 1
        self.a42 = -1
        self.a43 = 1

        self.c2 = 1 / 3
        self.c3 = 2 / 3
        self.c4 = 1

        self.b1 = 1 / 8
        self.b2 = 3 / 8
        self.b3 = 3 / 8
        self.b4 = 1 / 8

    def A(self, t):
        return np.block([[self.O, self.g(t)],
                         [self.f(t), self.O]])

    def step_setup(self):
        self.X = np.vstack((self.V, self.W))
        self.A0 = self.A4
        self.A2 = self.A(self.t + self.c2 * self.h)
        self.A3 = self.A(self.t + self.c3 * self.h)
        self.A4 = self.A(self.t + self.c4 * self.h)

    def step_body(self):
        K1 = self.A0 @ self.X
        K2 = self.A2 @ (self.X + self.h * self.a21 * K1)
        K3 = self.A3 @ (self.X + self.h * (self.a31 * K1 + self.a32 * K2))
        K4 = self.A4 @ (self.X + self.h *
                        (self.a41 * K1 + self.a42 * K2 + self.a43 * K3))

        self.X = (self.X + self.h *
                  (self.b1 * K1 + self.b2 * K2 + self.b3 * K3 + self.b4 * K4))
        self.V, self.W = np.split(self.X, 2)
