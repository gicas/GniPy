import numpy as np
from numpy.random import rand
import scipy as sp
import scipy.linalg as la
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib2tikz import save as savetikz


def sympj(n):
    return np.block([[np.zeros((n, n)), np.eye(n)],
                     [-1 * np.eye(n), np.zeros((n, n))]])


def printfigs(name):
    plt.savefig(name + '.eps', format='eps')
    savetikz(name + '.tikz', show_info=False)


def compare(reference, methods, sys, iv, tspan, steps, name='', isvec=True):
    errors = np.empty((len(methods), len(steps)))
    costs = np.empty((len(methods), len(steps)))
    for idm, m in enumerate(methods):
        for ids, s in enumerate(steps):
            integrator = m(sys, iv, tspan, nsteps=s)
            result = integrator()
            errors[idm, ids] = la.norm(np.nan_to_num(reference - result), 2)
            if isvec:
                costs[idm, ids] = s * integrator.veccost
            else:
                costs[idm, ids] = s * integrator.matcost

    lgdnames = [met.texname for met in methods]
    pltname = 'efficiency_' + name
    fig = plt.figure()
    for m, met in enumerate(methods):
        plt.plot(np.log10(costs[m, :]),
                 np.log10(errors[m, :]+1e-15), **met.plotstyle)
    plt.ylim([-15, 0])
    plt.xlim([2, 5])
    plt.title(r'Methods efficiency')
    plt.xlabel(r'$\log_{10} cost$')
    plt.ylabel(r'$\log_{10} error$')
    plt.legend(lgdnames, loc='upper left')
    plt.tight_layout()
    printfigs(pltname)
    plt.close(fig)
    return costs, errors


def sympmat(methods, sys, iv1, iv2, tspan, nsteps, name):
    shp = sys[0](0).shape
    dim = shp[0]
    symperr = np.zeros((nsteps, len(methods)))
    J = sympj(dim)
    u = np.block([np.eye(dim), np.zeros(shp)])
    v = np.block([np.zeros(shp), np.eye(dim)])
    for m, met in enumerate(methods):
        igu = met(sys, (u, v), tspan, nsteps)
        for s in range(nsteps):
            sol = igu.step()
            smp = sol.T @ J @ sol
            symperr[s, m] = la.norm(np.nan_to_num(smp - J) + 1.e-16)

    xplt = tspan[0] + (tspan[-1] / nsteps) \
        * np.array(range(nsteps)).reshape(-1, 1)
    lgdnames = [met.texname for met in methods]
    pltname = 'symp_mat_' + 'nsteps_' + str(nsteps) + name
    fig = plt.figure()
    plt.plot(np.log10(xplt), np.log10(symperr))
    plth = np.round((tspan[-1] - tspan[0]) / nsteps, 2)
    plt.ylim([-16, -8])
    plt.title(r'Symplectic matrix error, $h\approx%s$' % plth)
    plt.xlabel(r'$\log_{10} t$')
    plt.ylabel(r'$\log_{10} |M^{T}JM-J|$')
    plt.legend(lgdnames, loc='upper left')
    plt.tight_layout()
    printfigs(pltname)
    plt.close(fig)


def sympvec(methods, sys, iv1, iv2, tspan, nsteps, name):
    dim = sys[0](0).shape[0]
    symperr = np.zeros((nsteps, len(methods)))
    J = sympj(dim)
    u = np.vstack(iv1)
    v = np.vstack(iv2)
    form = u.T @ J @ v
    for m, met in enumerate(methods):
        igu = met(sys, iv1, tspan, nsteps)
        igv = met(sys, iv2, tspan, nsteps)
        for s in range(nsteps):
            uc = igu.step()
            vc = igv.step()
            cform = uc.T @ J @ vc
            symperr[s, m] = la.norm(np.nan_to_num(form - cform) + 1.e-16)

    xplt = tspan[0] + (tspan[-1] / nsteps) \
        * np.array(range(nsteps)).reshape(-1, 1)
    lgdnames = [met.texname for met in methods]
    pltname = 'symp_vec_' + 'nsteps_' + str(nsteps) + name
    fig = plt.figure()
    plt.plot(np.log10(xplt), np.log10(symperr))
    plt.ylim([-16, -8])
    plth = np.round((tspan[-1] - tspan[0]) / nsteps, 2)
    plt.title(r'Symplectic form error, $h\approx%s$' % plth)
    plt.xlabel(r'$\log_{10} t$')
    plt.ylabel(r'$\log_{10} |\omega(Au,Av)-\omega(u,v)|$')
    plt.legend(lgdnames, loc='upper left')
    plt.tight_layout()
    printfigs(pltname)
    plt.close(fig)


def sympmatvec(methods, sys, iv1, iv2, tspan, nsteps, name='a'):
    shp = sys[0](0).shape
    dim = shp[0]
    symperr = np.zeros((nsteps, len(methods)))
    J = sympj(dim)
    m1 = np.block([np.eye(dim), np.zeros(shp)])
    m2 = np.block([np.zeros(shp), np.eye(dim)])
    u = np.vstack(iv1)
    v = np.vstack(iv2)
    form = u.T @ J @ v
    for m, met in enumerate(methods):
        ig = met(sys, (m1, m2), tspan, nsteps)
        for s in range(nsteps):
            M = ig.step()
            cform = u.T @ M.T @ J @ M @ v
            symperr[s, m] = la.norm(np.nan_to_num(form - cform) + 1.e-16)

    xplt = tspan[0] + (tspan[-1] / nsteps) \
        * np.array(range(nsteps)).reshape(-1, 1)
    lgdnames = [met.texname for met in methods]
    pltname = 'symp_mat_vec_' + 'nsteps_' + str(nsteps) + name
    fig = plt.figure()
    plt.plot(np.log10(xplt), np.log10(symperr + 1.e-15))
    plth = np.round((tspan[-1] - tspan[0]) / nsteps, 2)
    plt.ylim([-16, -8])
    plt.title(r'Symplectic matrix-vector error, $h\approx%s$' % plth)
    plt.xlabel(r'$\log_{10} t$')
    plt.ylabel(r'$\log_{10} |u^T M^{T}JMv-u^T J v|$')
    plt.legend(lgdnames, loc='upper left')
    plt.tight_layout()
    printfigs(pltname)
    plt.close(fig)


def mathieu_eig(methods):
    for method in methods:
        def mathieu(omega, eps):
            return lambda t: np.array(-(omega**2 + eps * sp.cos(2 * t))).reshape(-1, 1)

        def Idf(t): return np.identity(1)
        u = np.block([np.eye(1), np.zeros((1, 1))])
        v = np.block([np.zeros((1, 1)), np.eye(1)])
        omegas = np.arange(0, 5.1, 1 / 200)
        eps = 5
        errors = np.empty((len(omegas), 2))
        for om, omega in enumerate(omegas):
            Mf = mathieu(omega, eps)
            sol = method((Mf, Idf), (u, v), [0, sp.pi], 10)()
            errors[om, :] = np.abs(la.eigvals(sol)) - 1 + 10e-14

        errors = np.log10(np.abs(errors))
        fig = plt.figure()
        for k in range(2):
            plt.plot(omegas, errors[:, k], linestyle='-', markersize=0)
        plt.title(
            r'Error in the eigvals of Mathieu eq., %s, $h=\pi/10, t=[0,\pi]$' % method.texname)
        plt.xlabel(r'$\omega$')
        plt.ylabel(r'$\log_{10} \lambda_i - 1|$')
        plt.legend([method.texname])
        plt.tight_layout()
        pltname = 'mathieu_eig_' + method.name
        printfigs(pltname)
        plt.close(fig)


def mathieu_omega(methods):
    def mathieu(omega, eps):
        return lambda t: np.array(-(omega**2 + eps * sp.cos(2 * t))).reshape(-1, 1)

    def Idf(t): return np.identity(1)
    omegas = np.arange(0, 10, 0.1)
    errors = np.empty((len(omegas), len(methods)))
    for m, method in enumerate(methods):
        u = np.block([np.eye(1), np.zeros((1, 1))])
        v = np.block([np.zeros((1, 1)), np.eye(1)])
        eps = 5
        for om, omega in enumerate(omegas):
            Mf = mathieu(omega, eps)
            reference = methods[0]((Mf, Idf), (u, v), [0, sp.pi], 500)()
            sol = method((Mf, Idf), (u, v), [0, sp.pi], 20)()
            errors[om, m] = la.norm(sol - reference)

    errors = np.log10(np.abs(errors))
    fig = plt.figure()
    plt.plot(omegas, errors, linestyle='-', markersize=0)
    plt.title(r'Error in the solution of Mathieu eq., $h=\pi/20, t=[0,\pi]$')
    plt.xlabel(r'$\omega$')
    plt.ylabel(r'$\log_{10} ||error||$')
    lgdnames = [met.texname for met in methods]
    plt.legend(lgdnames, loc='upper left')
    plt.tight_layout()
    pltname = 'mathieu_omega'
    printfigs(pltname)
    plt.close(fig)
