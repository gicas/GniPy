import numpy as np
from core import Integrator, GaussLegendreInterp, GLI4, GLI6
from scipy import linalg as la
from expms import expmkr


def eyes(shp):
    m, n = shp
    return np.eye(m, n)


class MagnusDecomposition(Integrator, GaussLegendreInterp):
    def step_setup(self):
        self.M = [self.f(self.t + qn * self.h) for qn in self.qnodes]
        self.N = [self.g(self.t + qn * self.h) for qn in self.qnodes]
        self.K = self.M[2] - self.M[0]
        self.L = self.M[0] - 2 * self.M[1] + self.M[2]
        self.F = self.h ** 2 * self.K @ self.K


class Ups41(MagnusDecomposition, GLI6):
    texname = r'$\Upsilon_{1}^{[4]}$'
    name = r'Ups41'
    line = '--'
    plotstyle = dict(linestyle=line, marker='o', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(Ups41, self).__init__(*args, **kwargs)
        self.matcost = (7. + 8. + 1. / 3.) + 2
        self.veccost = (7. + 1. / 3.) + 1

    def step_body(self):
        self.C1 = (-np.sqrt(15) / 36) * self.K + (5 / 36) * self.L
        self.C2 = (+np.sqrt(15) / 36) * self.K + (5 / 36) * self.L
        self.D1 = self.M[1]

        self.W += self.h * self.C1 @ self.V
        self.result = np.vstack((self.V, self.W))
        self.step_exp()
        self.V, self.W = np.split(self.result, 2)
        self.W += self.h * self.C2 @ self.V

    def step_exp(self):
        T1 = self.h * np.block([[self.O, self.Id],
                                [self.D1, self.O]])
        self.result = la.expm(T1) @ self.result


class Ups41Kr(Ups41):
    texname = texname = r'$\Upsilon_{1}^{[4]}$'
    name = r'Ups41Kr'
    line = '--'
    plotstyle = dict(linestyle=line, marker='o', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(Ups41Kr, self).__init__(*args, **kwargs)
        self.matcost = None
        self.veccost = 1

    def step_exp(self):
        T1 = self.h * np.block([[self.O, self.Id],
                                [self.D1, self.O]])
        self.result, cost = expmkr(T1, self.result)
        self.veccost += cost / self.nsteps


class Ups62(MagnusDecomposition, GLI6):
    texname = r'$\Upsilon_{2}^{[6]}$'
    name = r'Ups62'
    line = '-.'
    plotstyle = dict(linestyle=line, marker='^', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(Ups62, self).__init__(*args, **kwargs)
        self.matcost = 2. * (7. + 8. + 1. / 3.) + 2
        self.veccost = 2 * (7. + 1. / 3.) + 1

    def step_body(self):
        self.C1 = ((-np.sqrt(15) / 180) * self.K +
                   (1 / 18) * self.L + (1 / 12960) * self.F)
        self.C2 = ((+np.sqrt(15) / 180) * self.K +
                   (1 / 18) * self.L + (1 / 12960) * self.F)
        self.D1 = (self.M[1] - (4 / (3 * np.sqrt(15))) *
                   self.K + (1 / 6) * self.L)
        self.D2 = (self.M[1] + (4 / (3 * np.sqrt(15))) *
                   self.K + (1 / 6) * self.L)

        self.W += self.h * self.C1 @ self.V
        self.result = np.vstack((self.V, self.W))
        self.step_exp()
        self.V, self.W = np.split(self.result, 2)
        self.W += self.h * self.C2 @ self.V

    def step_exp(self):
        T1 = 0.5 * self.h * np.block([[self.O, self.Id],
                                      [self.D1, self.O]])
        self.result = la.expm(T1) @ self.result
        T2 = 0.5 * self.h * np.block([[self.O, self.Id],
                                      [self.D2, self.O]])
        self.result = la.expm(T2) @ self.result


class Ups62Kr(Ups62):
    texname = r'$\Upsilon_{2-Kr}^{[6]}$'
    name = r'Ups62Kr'
    line = (0, (3, 5, 1, 5, 1, 5))
    plotstyle = dict(linestyle=line, marker='^', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(Ups62Kr, self).__init__(*args, **kwargs)
        self.matcost = None
        self.veccost = 1

    def step_exp(self):
        T1 = 0.5 * self.h * np.block([[self.O, self.Id],
                                      [self.D1, self.O]])
        self.result, cost = expmkr(T1, self.result)
        self.veccost += cost / self.nsteps
        T2 = 0.5 * self.h * np.block([[self.O, self.Id],
                                      [self.D2, self.O]])
        self.result, cost = expmkr(T2, self.result)
        self.veccost += cost / self.nsteps


class Ups63(MagnusDecomposition, GLI6):
    texname = r'$\Upsilon_{3}^{[6]}$'
    name = r'Ups63'
    line = '-.'
    plotstyle = dict(linestyle=line, marker='d', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(Ups63, self).__init__(*args, **kwargs)
        self.matcost = 3 * (7 + 8 + 1 / 3.) + 2
        self.veccost = 3 * (7 + 1 / 3.) + 1
        self.x = [0.015446203250883929564,
                  0.56704071886547742757,
                  0.15679795546721757294,
                  0.085748160282456073452,
                  -0.13408143773095485515,
                  -0.088162987231578813572]

    def step_body(self):
        self.C1 = -np.sqrt(5 / 3) * self.x[0] * self.K
        self.C2 = +np.sqrt(5 / 3) * self.x[0] * self.K
        self.D1 = (self.M[1] - np.sqrt(5 / 3) * self.x[2] / self.x[1] * self.K
                   + (10 / 3) * self.x[3] / self.x[1] * self.L)
        self.D2 = self.M[1] + (10 / 3) * self.x[5] / self.x[4] * self.L
        self.D3 = (self.M[1] + np.sqrt(5 / 3) * self.x[2] / self.x[1] * self.K
                   + (10 / 3) * self.x[3] / self.x[1] * self.L)

        self.W += self.h * self.C1 @ self.V
        self.step_exp()
        self.V, self.W = np.split(self.result, 2)
        self.W += self.h * self.C2 @ self.V

    def step_exp(self):
        T1 = self.x[1] * self.h * np.block([[self.O, self.Id],
                                            [self.D1, self.O]])
        self.result = la.expm(T1) @ np.vstack((self.V, self.W))
        T2 = self.x[4] * self.h * np.block([[self.O, self.Id],
                                            [self.D2, self.O]])
        self.result = la.expm(T2) @ self.result
        T3 = self.x[1] * self.h * np.block([[self.O, self.Id],
                                            [self.D3, self.O]])
        self.result = la.expm(T3) @ self.result


class Ups63Kr(Ups63):
    texname = r'$\Upsilon_{3-Kr}^{[6]}$'
    name = r'Ups63kr'
    line = (0, (3, 5, 1, 5, 1, 5))
    plotstyle = dict(linestyle=line, marker='d', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(Ups63Kr, self).__init__(*args, **kwargs)
        self.matcost = None
        self.veccost = 1

    def step_exp(self):
        T1 = self.x[1] * self.h * np.block([[self.O, self.Id],
                                            [self.D1, self.O]])
        self.result, cost = expmkr(T1, np.vstack((self.V, self.W)))
        self.veccost += cost / self.nsteps
        T2 = self.x[4] * self.h * np.block([[self.O, self.Id],
                                            [self.D2, self.O]])
        self.result, cost = expmkr(T2, self.result)
        self.veccost += cost / self.nsteps
        T3 = self.x[1] * self.h * np.block([[self.O, self.Id],
                                            [self.D3, self.O]])
        self.result, cost = expmkr(T3, self.result)
        self.veccost += cost / self.nsteps


class Ups63Comm(MagnusDecomposition, GLI6):
    texname = r'$\Upsilon_{3C}^{[6]}$'
    name = r'Ups63Comm'
    line = '-.'
    plotstyle = dict(linestyle=line, marker='s', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(Ups63Comm, self).__init__(*args, **kwargs)
        self.matcost = 2 * (7 + 8 + 1 / 3.) + (4 + 4 + 1 / 3.) + 2
        self.veccost = 2 * (7 + 1 / 3) + (4 + 1 / 3) + 1
        self.x = [-0.014854314511050557757,  # x12 x0
                  1 / 60,                    # x13 x1
                  0.5,                       # x21 x2
                  -0.14058274195579776897,   # x22 x3
                  1 / 40,                    # x23 x4
                  0.0018123521556161089100]  # c11 x5

    def step_exp(self):
        T1 = self.h * self.x[2] * np.block([[self.O, self.Id],
                                            [self.D1, self.O]])
        self.result = la.expm(T1) @ self.result
        self.result = la.expm(self.E) @ self.result
        T2 = self.h * self.x[2] * np.block([[self.O, self.Id],
                                            [self.D2, self.O]])
        self.result = la.expm(T2) @ self.result

    def step_body(self):

        self.C2 = (-np.sqrt(5 / 3) * self.x[0] * self.K
                   + (10 / 3) * self.x[1] * self.L)
        self.D2 = (self.M[1] - np.sqrt(5 / 3) * self.x[3] / self.x[2] * self.K
                   + (10 / 3) * self.x[4] / self.x[2] * self.L)
        self.E = (np.sqrt(5 / 3) * self.x[5] * self.h * self.h
                  * np.block([[self.K, self.O], [self.O, -self.K]]))
        self.D1 = (self.M[1] + np.sqrt(5. / 3) * self.x[3] / self.x[2]
                   * self.K + (10 / 3) * self.x[4] / self.x[2] * self.L)
        self.C1 = (np.sqrt(5 / 3) * self.x[0] * self.K
                   + (10 / 3) * self.x[1] * self.L)

        self.W += self.h * self.C1 @ self.V
        self.result = np.vstack((self.V, self.W))
        self.step_exp()
        self.V, self.W = np.split(self.result, 2)
        self.W += self.h * self.C2 @ self.V


class Ups63CommKr(Ups63Comm):
    texname = r'$\Upsilon_{3C-Kr}^{[6]}$'
    name = r'Ups63CommKr'
    line = (0, (3, 5, 1, 5, 1, 5))
    plotstyle = dict(linestyle=line, marker='s', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(Ups63CommKr, self).__init__(*args, **kwargs)
        self.matcost = None
        self.veccost = 1

    def step_exp(self):
        T1 = self.h * self.x[2] * np.block([[self.O, self.Id],
                                            [self.D1, self.O]])
        self.result, cost = expmkr(T1, self.result)
        self.veccost += cost / self.nsteps
        self.result, cost = expmkr(self.E, self.result)
        self.veccost += cost / self.nsteps
        T2 = self.h * self.x[2] * np.block([[self.O, self.Id],
                                            [self.D2, self.O]])
        self.result, cost = expmkr(T2, self.result)
        self.veccost += cost / self.nsteps
