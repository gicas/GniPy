import krypy as kp
import numpy as np
from scipy.special import jv, jn
from scipy import exp
from scipy import linalg as la


def expmkr(A, b, maxiter=30, tol=1.e-15):
    arnd = kp.utils.Arnoldi(A, b, maxiter=maxiter)
    errest = float('inf')
    m = 0
    beta = la.norm(b, 2)
    if not arnd.invariant:
        while not arnd.invariant:
            try:
                arnd.advance()
                V, H = arnd.get()
                Vl, Hl = arnd.get_last()
                m = np.amin(H.shape)
                V = V[:, 0:m]
                H = H[0:m, 0:m]
                expmH = la.expm(H)
                expm1H = expmH  # (expmH - np.eye(m)) @ la.pinv(H)
                latest_h = Hl[-1, 0]
                errest = latest_h * np.abs(beta * expm1H[-1, 0])
                if errest < tol or m > maxiter:
                    break
            except Exception as e:
                break
        res = beta * V @ expmH
        subsp_dim = m
    else:
        return b, 1
    return res[:, [0]], subsp_dim


def expmcheb(A, v, maxiter=11):
    m, n = A.shape
    egv = la.eigvals(A)
    a = min(egv)
    b = max(egv)

    X = A

    dk = np.zeros((len(v), maxiter + 2))
    for k in range(maxiter - 1, -1, -1):
        ck = (1j)**k * jv(k, -1j)
        dk[:, [k]] = ck * v + 2 * X@dk[:, [k + 1]] - dk[:, [k + 2]]

    res = (dk[:, [0]] - dk[:, [2]])
    return res, maxiter
