import numpy as np
import scipy as sp
import numpy.fft as dft


def idf(n):
    return lambda t: np.eye(n)


def mathieu(omega, eps):
    return lambda t: np.array(-(omega**2 + eps * sp.cos(2 * t))).reshape(-1, 1)


def hill():
    pass


def wave(x, eps, delta, omega=1):
    h = np.abs(x[0] - x[1])
    n = len(x)
    L = (np.diagflat(-2 * np.ones((n, 1)), 0)
         + np.diagflat(np.ones((n - 1, 1)), 1)
         + np.diagflat(np.ones((n - 1, 1)), -1)
         + np.diagflat(1, n - 1) + np.diagflat(1, -n + 1))
    L = L / (h * h)

    def g(x, t):
        return (omega**2 + eps * np.cos(delta * t)) * x**2

    return lambda t: L - np.diagflat(g(x, t))


def kleingordon(x, eps, delta, omega=1):
    h = np.abs(x[0] - x[1])
    n = len(x)
    L = (np.diagflat(-2 * np.ones((n, 1)), 0)
         + np.diagflat(np.ones((n - 1, 1)), 1)
         + np.diagflat(np.ones((n - 1, 1)), -1)
         + np.diagflat(1, n - 1) + np.diagflat(1, -n + 1))
    L = L / (h * h)

    def g(x, t):
        return delta**2 / (1 + t)**2

    return lambda t: L - np.diagflat(g(x, t))


def walkerpreston(x, u):
    N = len(x)
    m = 1745
    D = 0.2251
    A = 0.011025
    w = 0.01787
    a = 1.1741

    def T(x, u):
        k = np.array(list(range(0, N // 2)) + list(range(-N // 2, 0)))
        k = (2 * sp.pi * 1j * k / (N * (x[1] - x[0])))**2
        fftder = dft.ifft(dft.fft(u) * k)
        return -1 / (2 * m) * fftder

    def V(x):
        return D * (1 - np.exp(-a * x))**2

    def f(t):
        return A * np.cos(w * t)

    return lambda t: T(x, u) + (V(x) + f(t) * x) * u


def phi(x):
    m = 1745
    D = 0.2251
    a = 1.1741
    w0=a*sp.sqrt(2*D/m)
    g = 2*D/w0
    phi = np.exp(-(g-1/2)*a*x)*np.exp(-g*np.exp(-a*x))
    return phi/sp.linalg.norm(phi)