from expms import expmkr
import numpy as np
import scipy as sp
from scipy import linalg as la


def eyes(shp):
    m, n = shp
    return np.eye(m, n)


def block(lst):
    return np.array(np.bmat(lst))


class Splitting:
    def __init__(self, sys, iv, tspan, nsteps, order=6):
        #  initial setup
        self.f, self.g = sys
        self.ivv, self.ivw = iv
        self.V, self.W = iv
        self.t0, self.tf = tspan
        self.nsteps = nsteps
        #  integration setup
        self.h = (self.tf - self.t0) / nsteps
        self.t = self.t0
        #  constants
        self.qr = [0.5 - np.sqrt(15.0) / 10.0, 0.5, 0.5 + np.sqrt(15.0) / 10.0]
        self.Id = eyes(self.f(0).shape)
        self.O = np.zeros(self.f(0).shape)

    def __call__(self):
        self.V, self.W = self.ivv, self.ivw
        self.integrate()
        return np.vstack((self.V, self.W))

    def step(self):
        raise NotImplemented

    def integrate(self):
#        print('Solving with ' + self.name)
        for s in range(self.nsteps):
            self.step()
        return np.vstack((self.V, self.W))


class RKNb611(Splitting):
    texname = r'$\mathrm{RKN_{b11}^{[6]}}$'
    name = 'RKN611'
    line = ':'
    plotstyle = dict(linestyle=line, marker='o', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(RKNb611, self).__init__(*args, **kwargs)
        self.veccost = 11
        self.matcost = 22

    def step(self):
        a1 = 0.123229775946271
        a2 = 0.290553797799558
        a3 = -0.127049212625417
        a4 = -0.246331761062075
        a5 = 0.357208872795928
        a6 = 1 - 2 * (a1 + a2 + a3 + a4 + a5)

        b1 = 0.0414649985182624
        b2 = 0.198128671918067
        b3 = -0.0400061921041533
        b4 = 0.0752539843015807
        b5 = -0.0115113874206879
        b6 = 1/2 - (b1 + b2 + b3 + b4 + b5)

        a = [0, a1, a2, a3, a4, a5, a6, a5, a4, a3, a2, a1]
        b = [b1, b2, b3, b4, b5, b6, b6, b5, b4, b3, b2, b1]

        tb = self.t
        ta = self.t
        for k in range(len(b)):
            self.V = self.V + a[k] * self.h * self.W
            ta = ta + a[k] * self.h
            self.W = self.W + b[k] * self.h * self.f(ta) @ self.V

        self.t = self.t + self.h
        return np.vstack((self.V, self.W))


class RKNb46(Splitting):
    texname = r'$\mathrm{RKN_{b6}^{[4]}}$'
    name = 'RKNb46'
    line = ':'
    plotstyle = dict(linestyle=line, marker='o', fillstyle='none')

    def __init__(self, *args, **kwargs):
        super(RKNb46, self).__init__(*args, **kwargs)
        self.veccost = 6
        self.matcost = 12

    def step(self):
        a1 = 0.245298957184271
        a2 = 0.604872665711080
        a3 = 1 / 2 - (a1 + a2)

        b1 = 0.0829844064174052
        b2 = 0.396309801498368
        b3 =-0.0390563049223486
        b4 = 1 - 2*(b1 + b2 + b3)

        a = [ 0, a1, a2, a3, a3, a2, a1]
        b = [b1, b2, b3, b4, b3, b2, b1]

        tb = self.t
        ta = self.t
        for k in range(len(b)):
            self.V = self.V + a[k] * self.h * self.W
            ta = ta + a[k] * self.h
            self.W = self.W + b[k] * self.h * self.f(ta) @ self.V

        self.t = self.t + self.h
        return np.vstack((self.V, self.W))
