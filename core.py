import numpy as np


def eyes(shp):
    m, n = shp
    return np.eye(m, n)


class Integrator(object):
    def __init__(self, sys, iv, tspan, nsteps):
        #  initial setup
        self.f, self.g = sys
        self.sys = sys
        self.ivf, self.ivg = np.copy(iv)
        self.V, self.W = None, None
        self.t0, self.tf = tspan
        self.nsteps = nsteps
        #  integration setup
        self.h = (self.tf - self.t0) / nsteps
        self.t = self.t0
        self.Id = eyes(self.f(0).shape)
        self.O = np.zeros(self.f(0).shape)

    def __call__(self):
        return self.integrate()

    # Method's step as Template Method
    def step_setup(self):
        raise NotImplementedError

    def step_body(self):
        raise NotImplementedError

    def step_end(self):
        self.t += self.h

    def step(self):
        self.step_setup()
        self.step_body()
        self.step_end()

    def integrate(self):
        self.V, self.W = self.ivf, self.ivg
        for k in range(self.nsteps):
            self.step()
        return self.getresult()

    def getresult(self):
        return np.vstack((self.V, self.W))


class GaussLegendreInterp:
    qnodes = None

class GLI4(GaussLegendreInterp):
    qnodes = np.array([1/2 - np.sqrt(3)/6,
                        1/2+np.sqrt(3)/6])

class GLI6(GaussLegendreInterp):
    qnodes = np.array([1 / 2 - np.sqrt(15) / 10,
                       1 / 2,
                       1 / 2 + np.sqrt(15) / 10])
