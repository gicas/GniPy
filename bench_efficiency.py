from scipy import pi, sin
import numpy as np
from rk import rk76, rkgl
from split import rkn
from magnusdecomp import ups62kr, ups63kr, ups63ckr, ups62ch
from magnussplit import msp
from desystems import wave, idf
from benchmarks import compare
import matplotlib.pyplot as plt

def bench_eff(dim, ord, eps, delta):
    x = np.linspace(-10, 10, dim)    
    desc = 'eps_' + str(eps) + "_delta_" + str(delta) + '_krord_' + str(ord)

    Mf = wave(x, eps, delta)
    Idf = idf(dim)
    sys = (Mf, Idf)
    #  initial values
    sigma = 1
    y = x - 1
    z = x + 20
    v = sigma * np.exp(-y*y/2).reshape(-1, 1) + \
        sigma*0.5 * np.exp(-0.5*z**2).reshape(-1, 1)
#    plt.plot(v)
#    plt.show()
    w = 0 * v
    iv = (v, w)
    #  time interval
    T0 = 0
    Tf = 2*pi/delta
    tspan = [T0, Tf]
    
    steps = np.array([4000, 6000, 8000, 10000,
                      12000]).astype(int)
    print(steps)
    print(Tf/steps)
    nsteps = int(Tf//pi * 50)
    print(nsteps)
    print(Tf/nsteps)
    methods = [ups62kr, msp]
    reference = msp(sys, iv, tspan, nsteps)()
    plt.plot(reference)
    plt.show()
    
    costs, errors = compare(reference, methods, sys, iv, tspan, steps, 
                            order=ord, name=desc, isvec=True)
    
    return costs/steps, errors